#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render

from store.models import Product
from store.models import Store


def home(request):
    """Render the landing page
    """
    product_list = Product.objects.all().order_by('-created_at')[:10]
    store_list = Store.objects.all().order_by('-created_at')[:10]

    return render(request, 'home.html', {
        'product_list': product_list,
        'store_list': store_list,
    })
