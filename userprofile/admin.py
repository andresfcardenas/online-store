#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin

from userprofile.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    """Admin for UserProfile model.
    """

    list_display = [
        'user',
    ]

admin.site.register(UserProfile, UserProfileAdmin)