#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.http import Http404
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from userprofile.forms import UserRegistrationForm
from store.models import Store
from store.models import Product


def user_login(request):
    """Logs in a user via ajax.
    """
    if request.user.is_authenticated():
        return HttpResponse('Usted ya está autenticado.')

    elif request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())

            return HttpResponseRedirect(reverse('userprofile:dashboard'))

    else:
        form = AuthenticationForm()

    return render(request, 'registration/login.html', {
        'form': form,
    })

    raise Http404


def ajax_register(request):
    """Show the registration form via ajax
    """
    if request.is_ajax():
        form = UserRegistrationForm()

        return render(request, 'registration/ajax_registration_form.html', {
            'form': form,
        })

    raise Http404


@login_required
def dashboard(request):
    """This view show process the list store
    """
    try:
        store = Store.objects.get(created_by=request.user)
    except ObjectDoesNotExist:
        store = False
    products_list = Product.objects.filter(store=store).exclude(quantity__lte=0)
    return render(request, 'userprofile/dashboard.html', {
        'store': store,
        'products_list': products_list,
        'slug': store,
    })