#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import patterns
from django.conf.urls import url

urlpatterns = patterns(
    'userprofile.views',

    # login
    url(
        r'^user-login/$',
        'user_login',
        name='user_login',
    ),

    # Ajax register
    url(
        r'^ajax-register/$',
        'ajax_register',
        name='ajax_register',
    ),

    # dashboard
    url(
        r'^dashboard/$',
        'dashboard',
        name='dashboard',
    ),
)