#! /usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models

from autoslug import AutoSlugField


class UserProfile(models.Model):
    """This model represent a user profile
    """
    user = models.OneToOneField(
        'auth.User',
    )

    slug = AutoSlugField(
        unique=True,
        always_update=True,
        populate_from=lambda instance: instance.user.get_full_name(),
    )

    def __unicode__(self):
        return u'{0} {1}'.format(self.user.first_name, self.user.last_name)